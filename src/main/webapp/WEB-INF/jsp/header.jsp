<%--
  Created by IntelliJ IDEA.
  User: firar
  Date: 10/4/2019
  Time: 5:39 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!-- Page Preloder -->
<div id="preloder">
    <div class="loader"></div>
</div>

<!-- Header section -->
<header class="header-section">
    <div class="header-warp">
        <div class="header-social d-flex justify-content-end">
            <p>Follow us:</p>
            <a href="#"><i class="fa fa-pinterest"></i></a>
            <a href="#"><i class="fa fa-facebook"></i></a>
            <a href="#"><i class="fa fa-twitter"></i></a>
            <a href="#"><i class="fa fa-dribbble"></i></a>
            <a href="#"><i class="fa fa-behance"></i></a>
        </div>
        <div class="header-bar-warp d-flex">
            <!-- site logo -->
            <a href="home.html" class="site-logo">
                <img src="${pageContext.servletContext.contextPath}/img/logo.png" alt="" >
            </a>
            <br/>
            <nav class="top-nav-area w-100">
                <div class="user-panel">
                    <a href="/logingregister">Giriş/Registrasiya</a>
                </div>
                <!-- Menu -->
                <ul class="main-menu primary-menu">
                    <li><a href="home.html">Əsas Səyifə</a></li>
                    <li><a href="games.html">Bütün bloglar</a>
                        <ul class="sub-menu">
                            <li><a href="game-single.html">Ən məhşur bloglar</a></li>
                        </ul>
                    </li>
                    <li><a href="review.html">İstifadəçilər</a></li>
                    <li><a href="blog.html">Janrlar</a></li>
                    <li><a href="contact.html">Bizim Kontaktimiz</a></li>
                </ul>
            </nav>
        </div>
    </div>
</header>
<!-- Header section end -->

