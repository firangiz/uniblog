<%--
  Created by IntelliJ IDEA.
  User: firar
  Date: 10/5/2019
  Time: 2:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<%--    <jsp:include page="meta.jsp"/>--%>
    <title>LOGIN|REGISTER</title>
<%--    <jsp:include page="css.jsp"/>--%>
    <script type="text/javascript" src="https://code.jquery.com/jquery-2.2.4.min.js"></script>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/loginregister.css"/>

    <style>
        body{
            position: absolute;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            font-family: "Roboto", sans-serif;
            background-color: #5356ad;
            overflow: hidden;
        }
    </style>
</head>
<body>
<%--<jsp:include page="header.jsp"/>--%>
<div class="container">
    <div class="box"></div>
    <div class="container-forms">
        <div class="container-info">
            <div class="info-item">
                <div class="table">
                    <div class="table-cell">
                        <p>
                            Uniblog hesabınız artıq var?
                        </p>
                        <div class="btn">
                            Giriş
                        </div>
                    </div>
                </div>
            </div>
            <div class="info-item">
                <div class="table">
                    <div class="table-cell">
                        <p>
                            Uniblog hesabınız yoxdur?
                        </p>
                        <div class="btn">
                            Registr
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-form">
            <div class="form-item log-in">
                <div class="table">
                    <div class="table-cell">
                        <h3 class="defect" style="text-align: center; color: rebeccapurple;">Login</h3>
                        <form action="/login" method="post">
                        <input name="username" placeholder="İstifadəçi ad" type="text" />
                        <input name="password" placeholder="Şifrə" type="password" />
                        <input class="btn" type="submit" value="Giriş">
                        </form>
                    </div>
                </div>
            </div>
            <div class="form-item sign-up">
                <div class="table">
                    <div class="table-cell">
                        <h3 class="defect" style="text-align: center; color: rebeccapurple;" >Registrasiya</h3>
                        <form action="/register" method="post">
                        <input name="email" placeholder="Email" type="text" />
                        <input name="mobile" placeholder="Mobile" type="number" />
                        <input name="username" placeholder="İstifadəçi adı" type="text" />
                        <input name="password" placeholder="Şifrə" type="password" />
                        <input type="submit" class="btn" value="Registr">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(".info-item .btn").click(function(){
        $(".container").toggleClass("log-in");
    });
    $(".container-form .btn").click(function(){
        $(".container").addClass("active");
        $(".defect").css("display", "none");
    });
</script>

<%--<jsp:include page="footer.jsp"/>--%>

<%--<jsp:include page="js.jsp"/>--%>
</body>
</html>