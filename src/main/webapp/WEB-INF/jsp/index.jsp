<%--
  Created by IntelliJ IDEA.
  User: firar
  Date: 10/4/2019
  Time: 2:38 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <jsp:include page="meta.jsp"/>
    <title>UNIBLOG</title>
<jsp:include page="css.jsp"/>
</head>
<body>
<jsp:include page="header.jsp"/>
<!-- Hero section -->
<section class="hero-section overflow-hidden">
    <div class="hero-slider owl-carousel">
        <div class="hero-item set-bg d-flex align-items-center justify-content-center text-center" data-setbg="../img/slider-bg-1.jpg">
            <div class="container">
                <h2 style="font-size: 100px">Öz blogunu yarad!</h2>
                <p>Bizim <br> saytda artıq 1542 bloglar vardır</p>
                <a href="#" class="site-btn">Postlar  <img src="${pageContext.servletContext.contextPath}/img/icons/double-arrow.png" alt="#"/></a>
            </div>
        </div>
        <div class="hero-item set-bg d-flex align-items-center justify-content-center text-center" data-setbg="../img/slider-bg-2.jpg">
            <div class="container">
                <h1 style="color: white;">Blogları oxumaq üçün müxtəlif janrlar var!</h1><br/>
                <a href="#" class="site-btn">Janrlar  <img src="${pageContext.servletContext.contextPath}/img/icons/double-arrow.png" alt="#"/></a>
            </div>
        </div>
    </div>
</section>
<!-- Hero section end-->



<br/><br/><br/>
<br/><br/><br/>
<br/><br/><br/>
<jsp:include page="footer.jsp"/>

<jsp:include page="js.jsp"/>
</body>
</html>
