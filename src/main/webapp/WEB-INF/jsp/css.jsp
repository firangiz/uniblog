<%--
  Created by IntelliJ IDEA.
  User: firar
  Date: 10/5/2019
  Time: 11:14 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<link href="${pageContext.servletContext.contextPath}/img/ikonka.png" rel="shortcut icon"/>
<!-- Google Font -->
<link href="https://fonts.googleapis.com/css?family=Roboto:400,400i,500,500i,700,700i,900,900i" rel="stylesheet">


<!-- Stylesheets -->
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css"/>
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/font-awesome.min.css"/>
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/slicknav.min.css"/>
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/owl.carousel.min.css"/>
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/magnific-popup.css"/>
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/animate.css"/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- Main Stylesheets -->
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/style.css"/>


<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
