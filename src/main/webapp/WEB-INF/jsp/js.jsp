<%--
  Created by IntelliJ IDEA.
  User: firar
  Date: 10/5/2019
  Time: 10:07 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<script src="${pageContext.servletContext.contextPath}/js/jquery-3.2.1.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/jquery.slicknav.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/owl.carousel.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/jquery.sticky-sidebar.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/jquery.magnific-popup.min.js" type="text/javascript"></script>
<script src="${pageContext.servletContext.contextPath}/js/main.js" type="text/javascript"></script>
