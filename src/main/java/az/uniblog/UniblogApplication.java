package az.uniblog;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UniblogApplication {

	public static void main(String[] args) {
		SpringApplication.run(UniblogApplication.class, args);
	}

}
