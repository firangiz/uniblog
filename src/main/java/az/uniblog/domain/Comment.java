package az.uniblog.domain;

import java.time.LocalDateTime;

public class Comment {
    private long id;
    private long commenterId;
    private long postId;
    private LocalDateTime idate;
    private LocalDateTime udate;
    private int status;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCommenterId() {
        return commenterId;
    }

    public void setCommenterId(long commenterId) {
        this.commenterId = commenterId;
    }

    public long getPostId() {
        return postId;
    }

    public void setPostId(long postId) {
        this.postId = postId;
    }

    public LocalDateTime getIdate() {
        return idate;
    }

    public void setIdate(LocalDateTime idate) {
        this.idate = idate;
    }

    public LocalDateTime getUdate() {
        return udate;
    }

    public void setUdate(LocalDateTime udate) {
        this.udate = udate;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Comment{" +
                "id=" + id +
                ", commenterId=" + commenterId +
                ", postId=" + postId +
                ", idate=" + idate +
                ", udate=" + udate +
                ", status=" + status +
                '}';
    }
}
