package az.uniblog.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller("/")
public class WebController {

    @GetMapping("/")
    public String getIndexPage(){
        return "/index";
    }

    @GetMapping("/logingregister")
    public String getLoginRegisterPage(){
        return "/login-register";
    }


    @PostMapping("/login")
    public void login(@RequestParam (name="username") String name, @RequestParam(name="password") String password){

    }
}
